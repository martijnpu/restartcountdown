package NL.martijnpu.RestartCountdown.bungee;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeMessages {
    static void sendMessage(ProxiedPlayer player, String message) {
        if (!message.isEmpty())
            message = BungeeFileManager.getMessage(message);

        if (player == null)
            sendConsole(message);
        else
            player.sendMessage(TextComponent.fromLegacyText(colorMessage(message, false)));
    }

    public static void sendConsole(String message) {
        Main.get().getLogger().info(ChatColor.translateAlternateColorCodes('&', message));
    }

    static void broadcast(String message) {
        if (!message.isEmpty())
            message = BungeeFileManager.getMessage(message);

        ProxyServer.getInstance().broadcast(new TextComponent(colorMessage(message, true)));
    }

    static String colorMessage(String message, boolean isServer) {
        if (message.isEmpty()) return "";
        return ChatColor.translateAlternateColorCodes('&', BungeeFileManager.getMessage(isServer ? "server" : "prefix") + message);
    }
}
