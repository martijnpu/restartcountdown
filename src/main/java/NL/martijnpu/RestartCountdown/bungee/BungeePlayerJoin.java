package NL.martijnpu.RestartCountdown.bungee;

import NL.martijnpu.RestartCountdown.Statics;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class BungeePlayerJoin implements Listener {
    BungeePlayerJoin() {
        Main.get().getProxy().getPluginManager().registerListener(Main.get(), this);
    }

    @EventHandler
    public void onPlayerJoin(PostLoginEvent e) {
        if (e.getPlayer().hasPermission("restartcountdown.restart") && (Statics.newVersion > Statics.currVersion) && NL.martijnpu.RestartCountdown.bukkit.Main.get().getConfig().getBoolean("update-notify", true)) {
            String text = "&6&m&l+-----------------&6&l= &eRestartCountdown &6&l=&6&m&l-----------------+&r"
                    + "\n&eNew version of Prefix available!"
                    + "\nCurrent version: &f" + Statics.currVersion
                    + "\n&eNew version: &f" + Statics.newVersion
                    + "\n&6&m&l+------------------------------------------+";
            BungeeMessages.sendMessage(e.getPlayer(), text);
        }
    }
}