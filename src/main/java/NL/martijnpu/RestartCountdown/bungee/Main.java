package NL.martijnpu.RestartCountdown.bungee;

import NL.martijnpu.RestartCountdown.Statics;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import org.bstats.bungeecord.Metrics;

public class Main extends Plugin {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        new BungeeFileManager();
        new BungeePlayerJoin();

        getProxy().getPluginManager().registerCommand(this, new CmdHandler(this));
        ProxyServer.getInstance().getScheduler().runAsync(this, () -> Statics.checkForUpdate(Double.parseDouble(getDescription().getVersion()), true));

        new Metrics(this, 17191);

        BungeeMessages.sendConsole("We're up and running");
    }

    @Override
    public void onDisable() {
        BungeeMessages.sendConsole("[" + this.getDescription().getName() + "] Disabled successful");
    }
}
