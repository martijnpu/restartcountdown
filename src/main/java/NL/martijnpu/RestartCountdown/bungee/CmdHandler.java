package NL.martijnpu.RestartCountdown.bungee;


import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static NL.martijnpu.RestartCountdown.bungee.BungeeMessages.*;

public class CmdHandler extends Command implements TabExecutor {
    private boolean isRunning;
    private final Main plugin;
    private int count;

    CmdHandler(Main plugin) {
        super("restartbungee", "", "");
        isRunning = false;
        this.plugin = plugin;
    }
    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        List<String> list = new ArrayList<>();

        if (!commandSender.hasPermission("restartcountdown.restart"))
            return list;

        if (args.length == 1) {
            list.add("help");
            list.add("abort");
            list.add("start");
            list.add("reload");
        }

        list.removeIf((x) -> !x.toLowerCase().startsWith(args[args.length - 1].toLowerCase()));
        Collections.sort(list);
        return list;
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        ProxiedPlayer player = (s instanceof ProxiedPlayer) ? (ProxiedPlayer) s : null;
        
        if(!s.hasPermission("restartcountdown.restart")) {
            sendMessage(player,"no-permission");
            return;
        }

        if(args.length == 0){
            args = new String[]{"help"};
        }

        switch (args[0].toLowerCase()) {
            case "help":
                sendHelpMessage(s);
                break;

            case "start":
                if(isRunning) {
                    sendMessage(player, "countdown.already-running");
                    return;
                }

                if(args.length == 1) {
                    count = 30;
                } else {
                    try {
                        count = Integer.parseInt(args[1]);
                    } catch (NumberFormatException ex) {
                        sendMessage(player, BungeeFileManager.getMessage("invalid").replaceAll("%AMOUNT%", args[1]));
                        return;
                    }

                    if(count < 10)
                        count = 10;
                }


                if(args.length >= 3) {
                    StringBuilder message = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        message.append(" ").append(args[i]);
                    }
                    broadcast(BungeeFileManager.getMessage("countdown.multiple-seconds").replaceAll("%AMOUNT%", count + "") +
                            "\n&6&l&m+----------------------------------------+" +
                            "\n&4&4" + message.toString() +
                            "\n&6&l&m+----------------------------------------+");
                    count--;
                }

                isRunning = true;
                ProxyServer.getInstance().getScheduler().schedule(plugin, this::countdown, 1, 1, TimeUnit.SECONDS);
                break;

            case "abort":
                if(isRunning) {
                    plugin.getProxy().getScheduler().cancel(plugin);
                    broadcast("countdown.aborted");
                    isRunning = false;
                } else {
                    sendMessage(player, "countdown.none");
                }
                break;

            case "reload":
                BungeeFileManager.reloadConfig();
                sendMessage(player, "reload");
                break;

            default:
                sendMessage(player, "unknown");
                break;
        }
    }


    private void countdown() {
        if((count <= 5) && (count >= 0)) {
            clearScreen();
        }

        if((count == 120) || (count == 90) || (count == 60) ||(count == 30) || (count == 15) || ((count <= 10) && (count > 1))) {
            broadcast(BungeeFileManager.getMessage("countdown.multiple-seconds").replaceAll("%AMOUNT%", count + ""));
        } else if(count == 1) {
            broadcast("countdown.single-second");
            broadcast("");
        } else if(count == 0) {
            broadcast("countdown.now");
            broadcast("countdown.now");
            broadcast("countdown.now");
        } else if(count == -1) {
            plugin.getProxy().getPluginManager().dispatchCommand(ProxyServer.getInstance().getConsole(), "restart");
            plugin.getProxy().getScheduler().cancel(plugin);
        }

        count--;
    }

    private static void clearScreen(){
        for (int x = 0; x < 20; x++){
            broadcast("");
        }
    }

    static void sendHelpMessage(CommandSender sender) {
        String helpMessage = ""
                + "&6&l&m+----------&6&l= &eRestartCountdown &6&l=&6&l&m----------+&r\n"
                + " &6➢ &a/restartbungee help &7- &eShow this menu\n"
                + " &6➢ &a/restartbungee start &7- &eStart a default countdown with 30 seconds\n"
                + " &6➢ &a/restartbungee start [seconds] [message] &7- &eStart a countdown with a custom time and message\n"
                + " &6➢ &a/restartbungee abort &7- &eStop the current countdown\n"
                + " &6➢ &a/restartbungee reload &7- &eReload the config\n"
                + "&r&6&l&m+----------------------------------------+";
        sender.sendMessage(TextComponent.fromLegacyText((ChatColor.translateAlternateColorCodes('&', helpMessage))));
    }
}