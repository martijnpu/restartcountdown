package NL.martijnpu.RestartCountdown.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class BukkitMessages {

    static void sendMessage(Player player, String message) {
        if (!message.isEmpty())
            message = Main.get().getConfig().getString("messages." + message, message);

        if (player == null)
            sendConsole(message);
        else
            player.sendMessage(colorMessage(message, false));
    }

    public static void sendConsole(String message) {
        Main.get().getLogger().info(ChatColor.translateAlternateColorCodes('&', message));
    }

    static void broadcast(String message) {
        if (!message.isEmpty())
            message = Main.get().getConfig().getString("messages." + message, message);

        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', colorMessage(message, true)));
    }

    static String colorMessage(String message, boolean isServer) {
        if (message.isEmpty()) return "";
        return ChatColor.translateAlternateColorCodes('&', Main.get().getConfig().getString(isServer ? "messages.server" : "messages.prefix") + message);
    }
}
