package NL.martijnpu.RestartCountdown.bukkit;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabComplete implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {

        List<String> tabSuggest = new ArrayList<>();

        if (!sender.hasPermission("restartcountdown.restart"))
            return tabSuggest;

        List<String> list= new ArrayList<>();

        if (args.length == 1) {
            list.add("help");
            list.add("abort");
            list.add("start");
            list.add("reload");
        }

        for (int i = list.size() - 1; i >= 0; i--) {
            try {
                if (list.get(i).toLowerCase().startsWith(args[args.length - 1].toLowerCase()))
                    tabSuggest.add(list.get(i));
            } catch (Exception ex) {
                tabSuggest.add("~Unknown item");
            }
        }

        Collections.sort(tabSuggest);
        return tabSuggest;
    }
}