package NL.martijnpu.RestartCountdown.bukkit;

import NL.martijnpu.RestartCountdown.Statics;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class Main extends JavaPlugin {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        new BukkitFileManager();
        new BukkitPlayerJoin();

        Objects.requireNonNull(getCommand("restartcountdown")).setExecutor(new CmdHandler(this));
        Objects.requireNonNull(getCommand("restartcountdown")).setTabCompleter(new TabComplete());
        Bukkit.getScheduler().runTaskAsynchronously(this, () -> Statics.checkForUpdate(Double.parseDouble(getDescription().getVersion()), false));

        new Metrics(this, 17188);

        BukkitMessages.sendConsole("[" + this.getDescription().getName() + "] We're up and running");
    }

    @Override
    public void onDisable() {
        BukkitMessages.sendConsole("[" + this.getDescription().getName() + "] Disabled successful");
    }
}
