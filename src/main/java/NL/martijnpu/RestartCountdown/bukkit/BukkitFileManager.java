package NL.martijnpu.RestartCountdown.bukkit;

import NL.martijnpu.RestartCountdown.Statics;

import java.io.File;
import java.util.Map;

import static NL.martijnpu.RestartCountdown.bukkit.BukkitMessages.sendConsole;

public class BukkitFileManager {

    BukkitFileManager() {
        Main.get().saveDefaultConfig();
        Main.get().reloadConfig();
        if (!Main.get().getConfig().isSet("File-Version-Do-Not-Edit")
                || !Main.get().getConfig().get("File-Version-Do-Not-Edit", "-1").equals(Statics.CONFIG_FILE_VERSION)) {
            sendConsole("Your config file is outdated!");
            sendConsole("Version: " + Main.get().getConfig().get("File-Version-Do-Not-Edit", "-1"));
            updateConfig();
            sendConsole("File successfully updated!");
        }
    }

    private void updateConfig() {
        Map<String, Object> values_old = Main.get().getConfig().getValues(true);
        values_old.put("File-Version-Do-Not-Edit", Statics.CONFIG_FILE_VERSION);


        File backupFile = new File(Main.get().getDataFolder(), "config_backup.yml");
        File configFile = new File(Main.get().getDataFolder(), "config.yml");
        int i = 1;

        while (backupFile.exists())
            backupFile = new File(Main.get().getDataFolder(), "config_backup_" + i++ + ".yml");

        configFile.renameTo(backupFile);
        sendConsole("Saved old config to \"" + backupFile.getName() + "\"!");

        Main.get().saveDefaultConfig();
        Main.get().reloadConfig();
        Map<String, Object> values_new = Main.get().getConfig().getValues(true);

        values_new.forEach((path, value) -> Main.get().getConfig().set(path, values_old.getOrDefault(path, value)));
        Main.get().saveConfig();
        Main.get().reloadConfig();
    }
}
