package NL.martijnpu.RestartCountdown;

import NL.martijnpu.RestartCountdown.bukkit.BukkitMessages;
import NL.martijnpu.RestartCountdown.bungee.BungeeMessages;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Statics {
    public final static int CONFIG_FILE_VERSION = 1;
    public static double newVersion;
    public static double currVersion;
    public static boolean isProxy;

    public static void checkForUpdate(double pluginVersion, boolean isProxy) {
        Statics.isProxy = isProxy;
        currVersion = pluginVersion;
        try {
            getSpigotVersion2();
            if (newVersion > pluginVersion) {
                sendConsole("------------------------------------------------");
                sendConsole("A new version of RestartCountdown is available.");
                sendConsole("New version: " + newVersion + ". Current version: " + pluginVersion + ".");
                sendConsole("URL: https://www.spigotmc.org/resources/restartcountdown.82460/");
                sendConsole("------------------------------------------------");
            } else {
                sendConsole("There is not a new version available for RestartCountdown.");
            }
        } catch (Exception e) {
            sendConsole("------------------------------------------------");
            sendConsole("Unable to receive version from RestartCountdown.");
            sendConsole("Current version: " + pluginVersion + ".");
            sendConsole("URL: https://www.spigotmc.org/resources/restartcountdown.82460/");
            sendConsole("------------------------------------------------");
            newVersion = 0;
        }
    }

    private static void getSpigotVersion2() throws IOException {
        final HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.spigotmc.org/legacy/update.php?resource=82460").openConnection();
        connection.setRequestMethod("GET");
        newVersion = Double.parseDouble(new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine());
    }

    static void sendConsole(String message) {
        if (isProxy)
            BungeeMessages.sendConsole(message);
        else
            BukkitMessages.sendConsole(message);
    }
}
